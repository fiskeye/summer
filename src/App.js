import React, { Component } from 'react'
import './App.css'
import ListPokemon from './components/ListPokemon'

import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchPokemons } from './actions'

// const COLORS = {
//   Psychic: "#f8a5c2",
//   Fighting: "#f0932b",
//   Fairy: "#c44569",
//   Normal: "#f6e58d",
//   Grass: "#badc58",
//   Metal: "#95afc0",
//   Water: "#3dc1d3",
//   Lightning: "#f9ca24",
//   Darkness: "#574b90",
//   Colorless: "#FFF",
//   Fire: "#eb4d4b"
// }

class App extends Component {

  static propTypes = {
    listAdd: PropTypes.array,
    dispatch: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.dispatch(fetchPokemons(''))
  }

  render() {
    const { listAdd } = this.props
    return (
      <div className="App" >
        {
          <ListPokemon list={listAdd !== undefined ? listAdd : []} />
        }
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { pokemonReducer } = state

  return {
    listAdd: pokemonReducer.listAdd,
  }
}

export default connect(mapStateToProps)(App)