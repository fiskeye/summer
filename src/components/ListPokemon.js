//ListPokemon

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Popup from "reactjs-popup"

import { connect } from 'react-redux'
import { fetchPokemons, searchPokemon, doClearItem } from '../actions'

import ListSearch from './ListSearch'

class ListPokemon extends Component {

	static propTypes = {
		list: PropTypes.array.isRequired,
		dispatch: PropTypes.func.isRequired
	}

	constructor(props) {
		super(props)

		this.searchWithWords = this.searchWithWords.bind(this)
		this.clearItem = this.clearItem.bind(this)
	}

	searchWithWords(e) {
		const keyword = e.target.value.trim()
		this.props.dispatch(searchPokemon(keyword))
	}

	clearItem(e) {
		this.props.dispatch(doClearItem(e))
	}

	render() {
		const { list } = this.props
		return (
			<div className="card">
				<div className="card-header text-center">
					My Pokemodex
          </div>
				<div className="card-body">
					<div className="row" style={{ overflowY: "scroll", maxHeight: "570px", height:'570px' }}>
						{
							list.map(e => (
								<div className="col-md-6" key={e.id}>
									<div className="card">
										<div className="card-body">
											<div className="row">
												<div className="col-sm-6">
													<img className="card-img-bottom" src={e.imageUrlHiRes} alt="" />
												</div>
												<div className="col-sm-6">

													<div className="d-flex">
														<div className="mr-auto p-2 card-title">
															<h3>{e.name}</h3>
														</div>
														<h6 className="p-2 main-color" onClick={() => {
															this.clearItem(e)
														}}>Clear</h6>
													</div>

													<div className="row">
														<div className="col-sm-6">
															<h6 className="card-title">HP</h6>
														</div>
														<div className="col-sm-6">
															<div className="progress">
																<div className="progress-bar progress-bar-orange" role="progressbar" style={{ width: e.newHp }} aria-valuemin="0" aria-valuemax="100"></div>
															</div>
														</div>
													</div>

													<div className="row">
														<div className="col-sm-6">
															<h6 className="card-title">STR</h6>
														</div>
														<div className="col-sm-6">
															<div className="progress">
																<div className="progress-bar progress-bar-orange" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
															</div>
														</div>
													</div>

													<div className="row">
														<div className="col-sm-6">
															<h6 className="card-title">WEAK</h6>
														</div>
														<div className="col-sm-6">
															<div className="progress">
																<div className="progress-bar progress-bar-orange" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
															</div>
														</div>
													</div>


												</div>
											</div>

										</div>
									</div>
								</div>
							))}

					</div>
				</div>

				<Popup
					position="top left"
					trigger={
						<div className="card-footer text-center add-button">
							<div>
								<h6 style={{ color: "white" }}>+</h6>
							</div>
						</div>}
					modal closeOnDocumentClick>

					<div className="App">
						<div className="card" style={{ border: "none" }}>
							<div className="card-body">
								<div className="row">
									<input type="tel" className="form-control input-lg"
										placeholder="Enter your name of pokemon"
										onChange={this.searchWithWords} />
									<ListSearch />
								</div>
							</div>

						</div>
					</div>

				</Popup>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		...state
	}
}

export default connect(mapStateToProps)(ListPokemon);

