
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { doAddItem } from '../actions';

class ListSearch extends Component {

	static propTypes = {
		results: PropTypes.array.isRequired,
		dispatch: PropTypes.func.isRequired,
	}

	constructor(props) {
		super(props)
		this.addToList = this.addToList.bind(this)
	}

	addToList(e) {
		this.props.dispatch(doAddItem(e))
	}

	render() {
		const { results } = this.props
		if (results === undefined || results === null) {
			return (<div />)
		} else {
			return (
				<div style={{ overflowY: "scroll", maxHeight: "600px", marginTop: "10px" }}>
					{
						results.map(e => (
							<div className="row search-item-space" key={e.id}>
								<div className="card">
									<div className="card-body">
										<div className="row" key={e.id}>
											<div className="col-sm-4">
												<img className="card-img-bottom" src={e.imageUrlHiRes} />
											</div>
											<div className="col-sm-6">
												<div className="d-flex">
													<div className="mr-auto p-2 card-title">
														<h3>{e.name}</h3>
													</div>
													<h5 className="p-2 main-color" onClick={() => {
														this.addToList(e)
													}}>Add</h5>
												</div>

												<div className="row">
													<div className="col-sm-4">
														<h5 className="card-title">HP</h5>
													</div>
													<div className="col-sm-6">
														<div className="progress">
															<div className="progress-bar progress-bar-orange" role="progressbar"
																style={{ width: e.newHp }} aria-valuemin="0" aria-valuemax="100"></div>
														</div>
													</div>
												</div>

												<div className="row">
													<div className="col-sm-4">
														<h5 className="card-title">STR</h5>
													</div>
													<div className="col-sm-6">
														<div className="progress">
															<div className="progress-bar progress-bar-orange" role="progressbar"
																aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
														</div>
													</div>
												</div>

												<div className="row">
													<div className="col-sm-4">
														<h5 className="card-title">WEAK</h5>
													</div>
													<div className="col-sm-6">
														<div className="progress">
															<div className="progress-bar progress-bar-orange" role="progressbar"
																aria-valuemin="0" aria-valuemax="100"></div>
														</div>
													</div>
												</div>


											</div>
										</div>
									</div>
								</div>
							</div>
						))}

				</div>
			)
		}

	}
}

const mapStateToProps = state => {
	const { pokemonReducer } = state

	return {
		results: pokemonReducer.searchs,
	}
}

export default connect(mapStateToProps)(ListSearch);
