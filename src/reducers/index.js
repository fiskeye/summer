import { combineReducers } from 'redux';
import { REQUEST_POKEMON, RECEIVE_POKEMON, ADD_ITEM, SEARCH_POKEMON, CLEAR_ITEM } from '../actions';

const initState = {
  pokemons: [],
  searchs: [],
  listAdd: [],
  keyword: ''
}

const findPokemon = (state, keyword) => {
  var name = keyword.toLowerCase()

  return state.pokemons.filter(e => {
    return !e.isAdded && e.name.toLowerCase().includes(name)
  })
}

const pokemonReducer = (state = initState, action) => {
  switch (action.type) {
    case REQUEST_POKEMON:
      return {
        ...state,
        isFetching: false,
      }
    case RECEIVE_POKEMON:
      return {
        ...state,
        isFetching: true,
        pokemons: action.list,
      }
    case ADD_ITEM:

      var pokemon = action.payload
      pokemon.isAdded = true

      return {
        ...state,
        listAdd: [...state.listAdd, pokemon], // or listAdd: state.listAdd.concat(action.newItem)
        searchs: state.searchs.filter(e => e.id !== pokemon.id)
      }

    case CLEAR_ITEM:
      const id = action.payload.id
      return {
        ...state,
        pokemons: state.pokemons.map(poke => poke.id === id ? { ...poke, isAdded: false } : poke),
        listAdd: state.listAdd.filter(e => e.id !== id)
      }

    case SEARCH_POKEMON:
      return {
        ...state,
        searchs: findPokemon(state, action.payload)
      }
    default:
      return state
  }
}




const rootReducer = combineReducers({
  pokemonReducer,
})

export default rootReducer;