
const baseURL = 'http://localhost:3030/api'

export const REQUEST_POKEMON = 'REQUEST_POKEMON'
export const RECEIVE_POKEMON = 'RECEIVE_POKEMON'
export const SEARCH_POKEMON = 'SEARCH_POKEMON'
export const ADD_ITEM = 'ADD_ITEM'
export const CLEAR_ITEM = 'CLEAR_ITEM'


const requestPokemon = (name) => ({
  type: REQUEST_POKEMON,
  keyword: name
});

const receivePokemon = (json) => ({
  type: RECEIVE_POKEMON,
  list: json.data
});

// fetch 
export const fetchPokemons = (name) => dispatch => {
  dispatch(requestPokemon(name))

  const url = `${baseURL}/cards?limit=20&name=${name}&type=normal`

  return fetch(url).then(response => response.json())
    .then(json => dispatch(receivePokemon(json)))
}

export const searchPokemon = (name) => dispatch => {
  dispatch({ type: SEARCH_POKEMON, payload: name})
}

// add
export const doAddItem = (item) => dispatch => {
  dispatch({ type: ADD_ITEM, payload: item })
}

export const doClearItem = (item) => dispatch => {
  dispatch({ type: CLEAR_ITEM, payload: item })
}